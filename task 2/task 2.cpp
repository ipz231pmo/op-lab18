﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <time.h>
double* coefs;
double polinome(double x, int n, int i = 0) {
	if (i == n) {
#ifdef _DEBUG
		printf("%f", coefs[i]);
		for (int i = 0; i < n; i++)
			printf(")");
		printf("\n");
#endif // _DEBUG
		return coefs[i];
	}
#ifdef _DEBUG
	printf("%f + x(", coefs[i]);
#endif // _DEBUG
	return coefs[i] + x * polinome(x, n, i + 1);
}
double polinomeNoRecursion(double x, int n) {
	double res = coefs[n];
	for (int i = n; i > 0; i--)
		res = res * x + coefs[i - 1];
	return res;
}
void randomizeCoefs(int n) {
	coefs = (double*)realloc(nullptr, sizeof(double) * (n+1));
	for (int i = 0; i <= n; i++)
		coefs[i] = rand()%401/100.-2.;
}
void printCoefs(int n) {
	printf("coefs = {");
	for (int i = 0; i <= n; i++) {
		printf("%f ", coefs[i]);
	}
	printf("}\n");
}
int main(){
	srand(time(0));
	int n; double x;	
	scanf("%d %lf", &n, &x);
	randomizeCoefs(n);
	printCoefs(n);
	printf("Res is %f\n", polinome(x, n));	;
	printf("Res without recursion is %f\n", polinomeNoRecursion(x, n));
	realloc(coefs, 0);
	return 0;
}