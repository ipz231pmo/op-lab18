﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define ARRAY_SIZE 20
int arr[ARRAY_SIZE];
int max(int a, int b) {
    if (a > b) return a;
    return b;
}
void randomiseArray() {
    for (int i = 0; i < ARRAY_SIZE; i++)
        arr[i] = rand() % 201 - 100;
}
void printArray() {
    printf("array = {");
    for (int i = 0; i < ARRAY_SIZE; i++)
        printf("%d ", arr[i]);
    printf("}\n");
}
int findMax(int start=0) {
    if (start == ARRAY_SIZE - 1) return arr[start];
    return max(arr[start], findMax(start + 1));
}
int main(){
    srand(time(0));
    randomiseArray();
    printArray();
    printf("Max Element is: %d\n", findMax());
    return 0;
}