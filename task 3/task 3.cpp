﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

double func(int n) {
    if (n == 1) { 
#ifdef _DEBUG
        printf("2/1\n");
#endif // _DEBUG
        return 2.; 
    }
    if (n % 2 == 0) {
#ifdef _DEBUG
        printf("%d/%d\n", n, n+1);
#endif // _DEBUG
        return (float)(n) / (n + 1) * func(n - 1);
    }
#ifdef _DEBUG
    printf("%d/%d\n", n+1, n);
#endif // _DEBUG
    return (float)(n + 1) / n * func(n-1);
}

int main(){
    int n;
    scanf("%d", &n);
    printf("%f", func(n));
}
